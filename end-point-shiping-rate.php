<?php
/**
 * This endPoint receive a quantity of products and return
 * the shipping price
 */

foreach ($_POST as $key => $value) {
    $_POST[$key] = htmlspecialchars($value);
}

if(!isset($_POST["quantity"]) || empty($_POST["quantity"]) || !is_numeric($_POST["quantity"])) {
    echo json_encode([
        "result" => "fail",
        "error" => "Quantity not specified"
    ]);
    exit();
}

$out = [
    "result" => "ok",
    "shippingCost" => 2
];

if ($_POST["quantity"] > 4 && $_POST["quantity"] <= 5) {
    $out["shippingCost"] = 3;
} elseif($_POST["quantity"] > 5) {
    $out["shippingCost"] = 5;
}

echo json_encode($out);
exit();
?>