<?php

$array = [
    [
        'guest_id' => 177,
        'guest_type' => 'crew',
        'first_name' => 'Marco',
        'middle_name' => null,
        'last_name' => 'Burns',
        'gender' => 'M',
        'guest_booking' => [
            [
                'booking_number' => 20008683,
                'ship_code' => 'OST',
                'room_no' => 'A0073',
                'start_time' => 1438214400,
                'end_time' => 1483142400,
                'is_checked_in' => true,
            ],
        ],
        'guest_account' => [
            [
                'account_id' => 20009503,
                'status_id' => 2,
                'account_limit' => 0,
                'allow_charges' => true,
            ],
        ],
    ],
    [
        'guest_id' => 10000113,
        'guest_type' => 'crew',
        'first_name' => 'Bob Jr ',
        'middle_name' => 'Charles',
        'last_name' => 'Hemingway',
        'gender' => 'M',
        'guest_booking' => [
            [
                'booking_number' => 10000013,
                'room_no' => 'B0092',
                'is_checked_in' => true,
            ],
        ],
        'guest_account' => [
            [
                'account_id' => 10000522,
                'account_limit' => 300,
                'allow_charges' => true,
            ],
        ],
    ],
    [
        'guest_id' => 10000114,
        'guest_type' => 'crew',
        'first_name' => 'Al ',
        'middle_name' => 'Bert',
        'last_name' => 'Santiago',
        'gender' => 'M',
        'guest_booking' => [
            [
                'booking_number' => 10000014,
                'room_no' => 'A0018',
                'is_checked_in' => true,
            ],
        ],
        'guest_account' => [
            [
                'account_id' => 10000013,
                'account_limit' => 300,
                'allow_charges' => false,
            ],
        ],
    ],
    [
        'guest_id' => 10000115,
        'guest_type' => 'crew',
        'first_name' => 'Red ',
        'middle_name' => 'Ruby',
        'last_name' => 'Flowers ',
        'gender' => 'F',
        'guest_booking' => [
            [
                'booking_number' => 10000015,
                'room_no' => 'A0051',
                'is_checked_in' => true,
            ],
        ],
        'guest_account' => [
            [
                'account_id' => 10000519,
                'account_limit' => 300,
                'allow_charges' => true,
            ],
        ],
    ],
    [
        'guest_id' => 10000116,
        'guest_type' => 'crew',
        'first_name' => 'Ismael ',
        'middle_name' => 'Jean-Vital',
        'last_name' => 'Jammes',
        'gender' => 'M',
        'guest_booking' => [
            [
                'booking_number' => 10000016,
                'room_no' => 'A0023',
                'is_checked_in' => true,
            ],
        ],
        'guest_account' => [
            [
                'account_id' => 10000015,
                'account_limit' => 300,
                'allow_charges' => true,
            ],
        ],
    ],
];

$keys = ["last_name", "account_id"];
$result = orderByKeys($array, $keys);
print_r($result);

/**
 * This function order an array by the keys you send
 * it does not matter the level of the key in the array
 * @param array $array Array to order
 * @param array $orderKeys Array with key names to order
 * @return array
 */
function orderByKeys(array $array, array $orderKeys)
{

    // If it is not an array there is not anything to compare
    if (!is_array($array)) {
        return $array;
    }

    // Sorting array
    uasort($array, function ($a, $b) use ($orderKeys) {
        // Evaluating the keys to order
        foreach ($orderKeys as $key) {
            $aValue = is_array($a) ? getValue($a, $key) : '';
            $bValue = is_array($b) ? getValue($b, $key) : '';

            if ($aValue !== null && $bValue !== null) {
                // Comparate values to know the position
                return strcasecmp($aValue, $bValue);
            } elseif ($aValue !== null) {
                return -1;
            } elseif ($bValue !== null) {
                return 1;
            }
        }
        return 0;
    });

    foreach ($array as &$value) {
        // If the value is an array we are going to check
        // if there is a orderKey to order it by the orderkey
        if (is_array($value)) {
            // Setting the ordered array
            $value = orderByKeys($value, $orderKeys);
        }
    }

    return $array;
}

/**
 * This function get the key value in the array
 * recursively until get it
 * @param array $array Array with position for look the key
 * @param string $key Key that we are looking for its value
 * @return null|string
 */
function getValue(array $array, string $key)
{
    if (is_array($array)) {
        foreach ($array as $k => $v) {
            if ($k === $key) {
                return $v;
            }
            if (is_array($v)) {
                $result = getValue($v, $key);
                if ($result !== null) {
                    return $result;
                }
            }
        }
    }
    return null;
}
