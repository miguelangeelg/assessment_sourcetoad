<?php

/**
 * This class is where we have common functions
 * for the project
 */
class Core
{

    /**
     * Commont set for the classes
     * @param string $field Field name to set value
     * @param $value Value to set to the field
     */
    public function setField($field, $value)
    {
        if (property_exists($this, $field)) {
            $this->{$field} = $value;
        }
    }

    /**
     * This function get the value of any field
     * @param string $field Field name to get the value
     */
    public function getField($field)
    {
        if (property_exists($this, $field)) {
            return $this->{$field};
        }
        return null;
    }

    /**
     * Set function, to set a bunch of field
     * through an array
     * @param array $array Array with key that are the field name and
     * the value to se in the field
     */
    public function setFields($array)
    {
        foreach ($array as $field => $value) {
            $this->setField($field, $value);
        }
    }

    /**
     * This function is a common function to return success responses
     * @param array $payload Array with information to return
     */
    public static function responseResult($payload = [])
    {
        return [
            "result" => "ok",
            "payload" => $payload
        ];
    }

    /**
     * This function is a common function to return error responses
     * @param string $error Message of the error
     * @param array $payload Array with information
     */
    public static function responseError($error = "", $payload = [])
    {
        return [
            "result" => "fail",
            "error" => $error,
            "payload" => $payload
        ];
    }
}

class Item extends Core
{

    protected int $id;
    protected string $name;
    protected int $quantity;
    protected float $price;

    public function __construct(int $id, string $name, int $quantity, float $price)
    {
        $this->id = $id;
        $this->name = $name;
        $this->quantity = $quantity;
        $this->price = $price;
    }
}

class Customer extends Core
{
    protected string $firstName;
    protected string $lastName;
    protected array $addresses;

    public function __construct()
    {
        $this->addresses = [];
    }

    /**
     * Function to add address to the array of address
     * @param Address $address
     */
    public function addAddress(Address $address): void
    {
        $this->addresses[] = $address;
    }

    /**
     * Function to get all address
     */
    public function getAddresses(): array
    {
        return Core::responseResult($this->addresses);
    }
}

class Address extends Core
{
    protected string $lineOne;
    protected string $lineTwo;
    protected string $city;
    protected string $state;
    protected string $zip;

    public function __construct(string $lineOne, string $lineTwo, string $city, string $state, string $zip)
    {
        $this->lineOne = $lineOne;
        $this->lineTwo = $lineTwo;
        $this->city = $city;
        $this->state = $state;
        $this->zip = $zip;
    }
}

class Cart extends Core
{
    protected Customer $customer;
    private array $items;
    private const TAX_RATE = 0.07;
    private const API_SHIPPING_COST = "http://localhost/assessment/end-point-shiping-rate.php";

    public function __construct() {}

    /**
     * Function to add items to the cart
     * @param Item $item
     */
    public function addItem(Item $item): void
    {
        $this->items[] = $item;
    }

    /**
     * Function to get items of the cart
     */
    public function getItems(): array
    {
        return Core::responseResult($this->items);
    }

    /**
     * Calculate sub total of the items in the cart
     */
    public function calculateSubTotal(): array
    {
        $subTotal = 0;
        foreach ($this->items as $item) {
            $subTotal += $item->getField("price") * $item->getField("quantity");
        }
        return Core::responseResult($subTotal);
    }

    /**
     * This function return all the items added to the cart
     * with the price and the amount of taxes and the shipping cost
     */
    public function totalDetailed(): array
    {
        $out = [];

        foreach ($this->items as $item) {
            $shippingCost = $this->calculateShippingCost($item);

            if ($shippingCost["result"] != "ok") {
                return Core::responseError("Error getting the shipping cost");
            }

            $taxAmoun = ($item->getField("price") * $item->getField("quantity")) * self::TAX_RATE;

            $out[] = [
                "name" => $item->getField("name"),
                "price" => $item->getField("price"),
                "quantity" => $item->getField("quantity"),
                "subTotal" => ($item->getField("price") * $item->getField("quantity")),
                "taxes" => $taxAmoun,
                "shippingCost" => $shippingCost["shippingCost"],
                "total" => ($item->getField("price") * $item->getField("quantity")) + $taxAmoun + $shippingCost["shippingCost"]
            ];
        }

        return Core::responseResult($out);
    }

    /**
     * Thus function return the total cost of the items
     * including taxes and shipping cost
     */
    public function totalCost(): array
    {
        $totalDetailed = $this->totalDetailed();

        if ($totalDetailed["result"] != "ok") {
            return $totalDetailed;
        }

        $totalDetailed = $totalDetailed["payload"];

        $total = array_sum(array_column($totalDetailed, "total"));

        return Core::responseResult($total);
    }

    /**
     * This function obtains from the api to get shipping cost 
     * the value depending on the quantity of products.
     * @param Item $item
     */
    private function calculateShippingCost(Item $item): array
    {
        $data = [
            "quantity" => $item->getField("quantity")
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, self::API_SHIPPING_COST);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($ch);

        if (curl_errno($ch)) {
            return Core::responseError(curl_error($ch));
        }

        curl_close($ch);

        return json_decode($response, true);
    }
}

Class Order extends Core
{
    protected Cart $cart;
    protected Address $shippingAddress;

    public function __construct(Cart $cart, Address $shippingAddress)
    {
        $this->cart = $cart;
        $this->shippingAddress = $shippingAddress;
    }

    /**
     * Return the shipping address in a string
     */
    public function getShippingAddress(): array 
    {
        $addressFields = [
            $this->shippingAddress->getField('lineOne'),
            $this->shippingAddress->getField('lineTwo'),
            $this->shippingAddress->getField('city'),
            $this->shippingAddress->getField('state'),
            $this->shippingAddress->getField('zip')
        ];
    
        $shippingAddress = implode(', ', $addressFields);
    
        return Core::responseResult($shippingAddress);
    }
}

$customer = new Customer();

$customerInformation = [
    "firstName" => "Luci",
    "lastName" => "Rodriguez"
];

// Setting customer information in the object
$customer->setFields($customerInformation);

// Addresses to be saved
$addresses = [
    [
        "lineOne" => "456 Elm St",
        "lineTwo" => "Apt 2B",
        "city" => "Springfield",
        "state" => "IL",
        "zip" => "62701"
    ],
    [
        "lineOne" => "789 Oak Avenue",
        "lineTwo" => "Suite 300",
        "city" => "San Francisco",
        "state" => "CA",
        "zip" => "94103"
    ],
    [
        "lineOne" => "1010 Maple Drive",
        "lineTwo" => "Building C",
        "city" => "Austin",
        "state" => "TX",
        "zip" => "78704"
    ]
];

// Adding addresses of the customer
foreach ($addresses as $addressInfo) {
    $customer->addAddress(new Address(
        $addressInfo["lineOne"],
        $addressInfo["lineTwo"],
        $addressInfo["city"],
        $addressInfo["state"],
        $addressInfo["zip"]
    ));
}

$cart = new Cart();

// Setting customer object to the cart object
$cart->setField("customer", $customer);

$items = [
    [
        "id" => 1,
        "name" => "computer",
        "quantity" => 1,
        "price" => 900
    ],
    [
        "id" => 2,
        "name" => "mouse",
        "quantity" => 2,
        "price" => 55
    ],
    [
        "id" => 3,
        "name" => "mouse pad",
        "quantity" => 1,
        "price" => 35.9
    ],
    [
        "id" => 4,
        "name" => "camera protector",
        "quantity" => 5,
        "price" => 4.5
    ],
];


// Adding items to the cart
foreach ($items as $item) {
    $cart->addItem(new Item($item["id"], $item["name"], $item["quantity"], $item["price"]));
}

// Adding the cart and the address to the order
$order = new Order($cart, new Address(
    "789 Oak Avenue",
    "Suite 300",
    "San Francisco",
    "CA",
    "94103"
));

echo "\n \n";
print("----------------------- Output solution ----------------------- \n");
echo "\n \n";
echo "Customer Addresses: \n";
$customerFirstName = $order->getField("cart")->getField("customer")->getField("firstName");
$customerLastName = $order->getField("cart")->getField("customer")->getField("lastName");
print_r("{$customerFirstName} {$customerLastName}");
echo "\n \n";
echo "Customer Addresses: \n";
print_r($order->getField("cart")->getField("customer")->getAddresses()["payload"]);
echo "\n \n";
echo "Items in Cart: \n";
print_r($order->getField("cart")->getItems()["payload"]);
echo "\n \n";
echo "Where Order Ships: \n";
print_r($order->getShippingAddress()["payload"]);
echo "\n \n";
echo "Cost of item in cart, including shipping and tax: \n";
print_r($order->getField("cart")->totalDetailed()["payload"]);
echo "\n \n";
echo "Subtotal for all items in the cart: \n";
print_r($order->getField("cart")->calculateSubTotal()["payload"]);
echo "\n \n";
echo "Total for all items in the cart: \n";
print_r($order->getField("cart")->totalCost()["payload"]);
echo "\n \n";